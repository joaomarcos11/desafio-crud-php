<?php

$conn = new PDO("mysql:host=localhost; dbname=testdb", "root", "");

$stmt = $conn->prepare("DELETE FROM members WHERE id = :ID");

$id = $_POST['id'];

$stmt->bindParam(":ID", $id);

$stmt->execute();

header('location: list.php');
?>