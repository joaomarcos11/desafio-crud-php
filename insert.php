<?php

include_once 'create_db.php';
include_once 'create_table.php';

$conn = new PDO("mysql:host=localhost; dbname=testdb", "root", "");

$stmt = $conn->prepare("INSERT INTO members (username, phone) VALUES(:USERNAME, :PHONE)");

$username = $_POST['username'];
$phone = $_POST['phone'];

$stmt->bindParam(":USERNAME", $username);
$stmt->bindParam(":PHONE", $phone);

$stmt->execute();

header('location: index.php');


?>