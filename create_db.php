<?php

include 'connect.php';

$dbsql = "CREATE DATABASE testdb";


if ($connection->query($dbsql) === TRUE) {
    echo "Database created successfully";
}
else {
    echo "Error creating database: ".$connection->error;
}

$connection->close();
?>