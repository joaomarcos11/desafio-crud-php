<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>List of members </h1>
<?php

    $conn = new PDO("mysql:dbname=testdb; host=localhost", "root", "");

    $stmt = $conn->prepare("SELECT * FROM members ORDER BY id");

    $stmt->execute();

    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($results as $row) {
        foreach ($row as $key => $value) {
?>
    <table>
        <tr>
            <th> <?php echo $key.":" ; ?> </th>
            <td> <?php echo $value ; ?> </td>
        </tr>
    </table>
<?php
    }
    echo  "================================<br/>";
}
?>
<hr>
<h2> Update member's data </h2>
    <form action="update.php" method="POST">
        <p>Do you wish to update a member's data?<p>
        Id: <input type="text" name="id">
        New username: <input type="text" name="username">
        New phone: <input type="text" name="phone">
        <input type="submit" value="Update">
    </form>
<hr>
<h2> Delete member by ID </h2>
    <form action="delete.php" method="POST">
        <p>Do you wish to delete a member?<p>
        Id: <input type="text" name="id">
        <input type="submit" value="Delete">
    </form>
<hr>
<a href="index.php"><button>Previous page<button></a>
</body>
</html>