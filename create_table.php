<?php

include 'connect.php';
$dbname = "testdb";

$conndb = new mysqli($dbserver, $dbuser, $dbpass, $dbname);

if ($conndb->connect_error){
    die("Error: ". $conndb->connect_error);
}

$sql = "CREATE TABLE members (
    id int(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(100),
    phone VARCHAR(15)
    )";

if ($conndb->query($sql) === TRUE) {
    echo "Table Members created successfully";
}
else {
    echo "Error creating table: ".$conndb->error;
}

$conndb->close();
?>