<?php

$conn = new PDO("mysql:host=localhost; dbname=testdb", "root", "");

$stmt = $conn->prepare("UPDATE members SET username = :USERNAME, phone = :PHONE WHERE id = :ID");

$id = $_POST['id'];
$username = $_POST['username'];
$phone = $_POST['phone'];

$stmt->bindParam(":USERNAME", $username);
$stmt->bindParam(":PHONE", $phone);
$stmt->bindParam(":ID", $id);

$stmt->execute();

header('location: list.php');
?>